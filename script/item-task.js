'use strict';

document.addEventListener('DOMContentLoaded', () => {
	loadInfoTaskFromLocalStorage();

	document.querySelector('.buttons__back').addEventListener('click', () => {
		saveToLocalStorage();
		window.open('./index.html', '_self');
	});

	document.querySelector('.buttons__save').addEventListener('click', saveToLocalStorage);

	document.querySelector('.todo__description-task').addEventListener('keyup', () => {
		if (this.scrollTop > 0) {
			this.style.height = this.scrollHeight + "px";
		};
	});
});

// download tasks from local storage
function loadInfoTaskFromLocalStorage() {
	const task = JSON.parse(localStorage.getItem('item-task'));
	const title = document.querySelector('.header__title');
	title.dataset.idTask = task.id;
	title.dataset.completed = task.completed;
	title.textContent = task.title;
	document.querySelector('.todo__description-task').value = task.description;
}

// save tasks to local storage
function saveToLocalStorage() {
	const title = document.querySelector('.header__title').textContent;
	const description = document.querySelector('.todo__description-task').value;
	const id = document.querySelector('.header__title').dataset.idTask;
	const completed = document.querySelector('.header__title').dataset.completed;

	const data = {
		id: id,
		title: title,
		description: description,
		completed: completed,
	};
	const task = [];
	task.push(data);
	window.localStorage.setItem('item-task', JSON.stringify(task));
};