'use strict';
// Request to server
async function requestToServer(id) {
	const url = `https://jsonplaceholder.typicode.com/todos/${id}`;
	const response = await fetch(url);
	if (response.ok) {
		const result = await response.json();
		const { id, title, completed } = result;
		const description = null;
		createTask(id, title, description, completed);
	}
}

function getTasksFromServer(quantityTask = 20) {
	for (let i = 1; i < quantityTask; i++) {
		requestToServer(i);
	};
};

// Add task
function addNewTask() {
	const title = document.querySelector('.todo__add-task-input').value;
	if (title) {
		let id = 1;
		while (checkId(id)) { id++ };
		const description = null;
		createTask(id, title, description);
		document.querySelector('.todo__add-task-input').value = '';
	};
};

// Delete task
function deleteTask(taskElementLi) {
	taskElementLi.remove();
}

// Open task
function openTask(id, title, description, completed) {
	const task = {
		id: id,
		title: title,
		completed: completed,
		description: description
	}
	localStorage.setItem('item-task', JSON.stringify(task));
	window.open('./task.html', '_self');
}

// Check the same id
function checkId(id) {
	const allTask = document.querySelectorAll('.todo__list-item');
	const allId = [];

	if (allTask.length > 0) {
		allTask.forEach((task) => {
			allId.push(+task.getAttribute('data-id-task'));
		});
	};
	return allId.includes(+id);
};

// Create Task
function createTask(id, title, description = null, completed = false) {
	if (checkId(id)) return;
	const li = document.createElement('li');
	li.classList.add('todo__list-item');
	li.classList.add('list-item');
	li.dataset.idTask = id;
	li.dataset.completed = completed;


	const iconIsDoneTask = document.createElement('span');
	iconIsDoneTask.classList.add('far');
	iconIsDoneTask.classList.add('list-item__icon-is-done-task');
	li.append(iconIsDoneTask);

	const textTask = document.createElement('span');
	textTask.classList.add('list-item__text-task');
	textTask.textContent = title;
	li.append(textTask);

	const iconDeleteTask = document.createElement('span');
	iconDeleteTask.classList.add('fas');
	iconDeleteTask.classList.add('fa-trash-alt');
	iconDeleteTask.classList.add('list-item__delete-task');
	li.append(iconDeleteTask);

	const descriptionTask = document.createElement('span');
	descriptionTask.classList.add('description-task');
	descriptionTask.textContent = description;
	descriptionTask.style.display = 'none';
	li.append(descriptionTask);

	document.querySelector('.todo__list').append(li);

	checkIfCompleted(li);
}

// check if completed
function checkIfCompleted(elementLi) {
	const isCompleted = elementLi.dataset.completed;
	const elementIcon = elementLi.querySelector('.list-item__icon-is-done-task');
	const elementTaskText = elementLi.querySelector('.list-item__text-task');

	if (isCompleted == 'false') {
		notCompletedTask(elementIcon);
		removeClassDone(elementTaskText);
	} else if (isCompleted == 'true') {
		completedTask(elementIcon);
		addClassDone(elementTaskText);
	};
};

// click to see if it's done
function onClickCheckIfCompleted(elementLi) {
	const isCompleted = elementLi.dataset.completed;
	const elementIcon = elementLi.querySelector('.list-item__icon-is-done-task');
	const elementTaskText = elementLi.querySelector('.list-item__text-task');

	if (isCompleted == 'false') {
		completedTask(elementIcon);
		addClassDone(elementTaskText);
		elementLi.dataset.completed = 'true';
	} else if (isCompleted == 'true') {
		notCompletedTask(elementIcon);
		removeClassDone(elementTaskText);
		elementLi.dataset.completed = 'false';
	};
};

// Complete the task
function completedTask(elementIcon) {
	elementIcon.classList.remove('fa-circle');
	elementIcon.classList.add('fa-check-circle');
};

// Activate the task
function notCompletedTask(elementIcon) {
	elementIcon.classList.remove('fa-check-circle');
	elementIcon.classList.add('fa-circle');
};

// Remove class 'done'
function removeClassDone(element) {
	const isDone = element.closest('.done');
	if (isDone) {
		isDone.classList.remove('done');
	};
}

// Add class 'done'
function addClassDone(element) {
	const isDone = element.closest('.done');
	if (isDone == null) {
		element.classList.add('done');
	};
}

// save tasks to local storage
function saveToLocalStorage() {
	const allTask = document.querySelectorAll('.todo__list-item');
	const dataToSave = [];
	allTask.forEach((li) => {
		const data = {};
		data.id = li.dataset.idTask;
		data.title = li.querySelector('.list-item__text-task').textContent;
		data.completed = li.dataset.completed;
		data.description = li.querySelector('.description-task').textContent;
		dataToSave.push(data);
	});
	window.localStorage.setItem('all-task', JSON.stringify(dataToSave));
};

// download tasks from local storage
function loadTaskFromLocalStorage(element) {
	const data = JSON.parse(window.localStorage.getItem(element));
	if (data && data.length > 0) {
		data.forEach((task) => {
			const { id, title, description, completed } = task;
			createTask(id, title, description, completed);
		});
	};
};

// Clear todo-list
function clearTodoList() {
	const allTask = document.querySelectorAll('.todo__list-item');
	allTask.forEach(task => task.remove());
};

// show all task
function showAllTask() {
	const allTask = document.querySelectorAll('.todo__list-item');
	if (allTask.length > 0) {
		allTask.forEach((task) => {
			task.style.display = 'flex';
		});
	};
};

// hide done task
function hideDoneTask() {
	const allTask = document.querySelectorAll('.todo__list-item');
	if (allTask.length > 0) {
		allTask.forEach((task) => {
			if (task.dataset.completed == 'true') {
				task.style.display = 'none';
			};
		});
	};
};