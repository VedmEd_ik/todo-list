'use strict';

document.addEventListener('DOMContentLoaded', () => {
	document.addEventListener('click', documentClick);
	loadTaskFromLocalStorage('item-task');
	loadTaskFromLocalStorage('all-task');
});

// delegation event 'click'
function documentClick(event) {
	const targetElement = event.target;

	// Click on the add task button
	if (targetElement.closest('.todo__add-task-icon')) {
		addNewTask();
	}
	// Click on the button to complete the task
	if (targetElement.closest('.list-item__icon-is-done-task')) {
		const li = targetElement.closest('.todo__list-item');
		onClickCheckIfCompleted(li);
	}
	// Click on the task text
	if (targetElement.closest('.list-item__text-task')) {
		const li = targetElement.closest('.todo__list-item')
		const id = li.dataset.idTask;
		const title = targetElement.textContent;
		const completed = li.dataset.completed;
		const description = li.querySelector('.description-task').textContent;
		saveToLocalStorage();
		openTask(id, title, description, completed);
	}
	// Click on the delete task icon
	if (targetElement.closest('.list-item__delete-task')) {
		const li = targetElement.closest('.todo__list-item');
		deleteTask(li);
	}
	// Click on the save task button
	if (targetElement.closest('.buttons__save')) {
		saveToLocalStorage();
	};
	// Click on the load task from server
	if (targetElement.closest('.buttons__load-task-from-server')) {
		getTasksFromServer();
	};
	// Click on the clear all task
	if (targetElement.closest('.buttons__clear-todo-list')) {
		clearTodoList();
	};
	// Click on button show all task
	if (targetElement.closest('.filter__all-task')) {
		showAllTask();
	};
	// Click on button hide done task
	if (targetElement.closest('.filter__hide-done-task')) {
		hideDoneTask();
	};
};